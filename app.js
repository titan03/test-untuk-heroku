const express = require('express');
const app = express()
const PORT = process.env.PORT || 3000
const logger = require('morgan');
const Router = require('./router');
require('./models');

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger('dev'))

if (process.env.NODE_ENV !== 'test')
    app.get('/', (req, res) => {
        res.send('<h1> Welcome to My App </h1>')
    })

app.use(Router)

app.listen(PORT, () => {
    console.log(`Server started on ${Date(Date.now())}`);
    console.log(`Server is listening on port ${PORT}`);
});