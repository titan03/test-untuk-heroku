'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model

  class Task extends Model { }

  Task.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value`
        },
      }
    },
    description: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value`
        },
        len: [2, 255]
      }
    },
    status: {
      type: DataTypes.BOOLEAN,
      validate: {
        notEmpty: {
          msg: `Please input value`
        },
        isIn: [[true, false]]
      }
    },
    due_date: {
      type: DataTypes.DATE,
      validate: {
        notEmpty: {
          msg: `Please input value`
        },
        isDate: true
      }
    }
  }, { sequelize })
  // const Task = sequelize.define('Task', {

  // }, {});
  Task.associate = function (models) {
    // associations can be defined here
  };
  return Task;
};