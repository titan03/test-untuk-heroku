const router = require('express').Router();
const Task = require('../controller/test');

router.post('/', Task.create)
router.get('/', Task.getAll)
router.put('/:id', Task.update)
router.delete('/:id', Task.delete)


module.exports = router
