const { Task } = require('../models');

class Tasks {

    static create(req, res) {
        const { title, description, status, due_date } = req.body
        Task.create({
            title,
            description,
            status,
            due_date
        }).then((task) => {
            res.status(201).json({
                status: true,
                data: {
                    task
                }
            })
        }).catch((err) => {
            res.status(500).json({
                status: false,
                error: err
            })
        });
    }

    static getAll(req, res) {
        Task.findAll()
            .then((task) => {
                res.status(200).json({
                    status: true,
                    data: {
                        task
                    }
                })
            }).catch((err) => {
                res.status(500).json({
                    status: false,
                    error: err
                })
            });
    }

    static update(req, res) {
        const updateById = req.params.id
        const { title, description, status, due_date } = req.body
        Task.update({ title, description, status, due_date }, { where: { id: updateById } })
            .then((task) => {
                res.status(200).json({
                    status: true,
                    data: {
                        amount: task
                    }
                })
            }).catch((err) => {
                res.status(500).json({
                    status: false,
                    error: err
                })
            });
    }

    static delete(req, res) {
        const deleteById = req.params.id
        Task.destroy({ where: { id: deleteById } })
            .then((task) => {
                res.status(200).json({
                    status: true,
                    data: {
                        amount: task
                    }
                })
            }).catch((err) => {
                res.status(500).json({
                    status: false,
                    error: err
                })
            });

    }
}

module.exports = Tasks
